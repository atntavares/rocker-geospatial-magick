# rocker-geospatial-magick

This image is based on [rocker/geospatial](https://hub.docker.com/r/rocker/geospatial/), a Docker-based Geospatial toolkit for R, built on versioned Rocker images. It adds support to `magick` R package through its [dependencies](https://www.imagemagick.org/script/index.php).

# Usage

Check out [this page](https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image).

